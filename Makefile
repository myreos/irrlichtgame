##
## Makefile for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
##
## Made by grandr_r
## Login   <grandr_r@epitech.net>
##
## Started on  Thu Apr 28 09:01:16 2016 grandr_r
## Last update Tue May 31 10:49:26 2016 
##

Target = indie_studio

Sources = src/main.cpp

OBJ=	$(Sources:.cpp=.o)

IrrlichtHome := irrlicht-1.8.3

BinPath = ./

USERCPPFLAGS =

USERCXXFLAGS = -O3 -ffast-math

USERLDFLAGS = -L/usr/lib $(IrrlichtHome)/irrKlang/bin/linux-gcc-64/libIrrKlang.so $(IrrlichtHome)/irrKlang/bin/linux-gcc-64/ikpMP3.so -pthread

CPPFLAGS = -I$(IrrlichtHome)/include -I/usr/X11R6/include -I$(IrrlichtHome)/irrKlang/include $(USERCPPFLAGS) -I./include/ -pthread
CXXFLAGS = $(USERCXXFLAGS)
LDFLAGS = $(USERLDFLAGS)

NAME = $(BinPath)$(Target)$(SUF)

CXX=	g++ -std=c++11

all:		$(NAME)

$(NAME) all_win32 static_win32: LDFLAGS += -L$(IrrlichtHome)/lib/$(SYSTEM) -lIrrlicht
$(NAME): LDFLAGS += -L/usr/X11R6/lib$(LIBSELECT) -lGL -lXxf86vm -lXext -lX11 -lXcursor

all_win32 clean_win32 static_win32: SYSTEM=Win32-gcc
all_win32 clean_win32 static_win32: SUF=.exe
static_win32: CPPFLAGS += -D_IRR_STATIC_LIB_
all_win32: LDFLAGS += -lopengl32 -lm
static_win32: LDFLAGS += -lgdi32 -lwinspool -lcomdlg32 -lole32 -loleaut32 -luuid -lodbc32 -lodbccp32 -lopengl32

$(NAME) clean_linux: SYSTEM=Linux

$(NAME):	$(OBJ)
		$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(OBJ) -o $(NAME) $(LDFLAGS)

clean: clean_linux clean_win32
	$(warning Cleaning...)

fclean: fclean_linux fclean_win32
	$(warning Full Cleaning...)

install:
	$(warning Nothing to do, dependencies are alredy installed...)

clean_linux clean_win32:
	@$(RM) $(OBJ)

fclean_linux fclean_win32:
	@$(RM) $(NAME) $(OBJ) src/*~ include/*~

re:	fclean all

.PHONY: all all_win32 static_win32 clean clean_linux clean_win32

ifeq ($(HOSTTYPE), x86_64)
LIBSELECT=64
endif
ifeq ($(HOSTTYPE), sun4)
LDFLAGS += -lrt
endif
