//
// Spawner.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Sun May 22 19:41:37 2016 
// Last update Sun May 29 17:56:58 2016 croayn_v
//

#ifndef SPAWNER_HPP_
# define SPAWNER_HPP_

#include <irrlicht.h>
#include <vector>
#include "GetFromFile.hpp"
#include "IEnemy.hpp"
#define MAX_ENEMY 10
#include "ICharacter.hpp"

class 		Spawner
{
  int		pos;
  int		isAlive;
  int		enemyNo;
  irr::scene::IAnimatedMesh   *mesh;
  irr::scene::IAnimatedMeshSceneNode *modelNode;
  std::vector<IEnemy *>		allEnemies;
  std::string 			_modelName;
  std::string			_name;
 public:
  Spawner(int other, std::string const & name, std::string const &modelName) : pos(other), isAlive(100), enemyNo(0), _name(name), _modelName(modelName) {};

  void	spawnEnemy(irr::scene::ISceneManager * smgr, irr::video::IVideoDriver *driver, t_map *map)
  {
    //IEnemy   *          enemy = new IEnemy();
    if (enemyNo > MAX_ENEMY || isAlive == false)
      return;
    allEnemies.push_back(new IEnemy());
    allEnemies[allEnemies.size() - 1]->setPos(pos);
    allEnemies[allEnemies.size() - 1]->addNewMesh(smgr, "ressources/" + _modelName +".md2", "ressources/" + _modelName + ".jpg", driver);
    //allEnemies[allEnemies.size() - 1]->setMeshPosition(irr::core::vector3df(-167.f, -45.f, -80.f));
    allEnemies[allEnemies.size() - 1]->setMeshPosition(irr::core::vector3df(modelNode->getPosition().X, -45, modelNode->getPosition().Z));
    allEnemies[allEnemies.size() - 1]->setMeshRotation(irr::core::vector3df(0.f, 180, 0));
    allEnemies[allEnemies.size() - 1]->setMeshAnim(irr::scene::EMAT_RUN);

    map[allEnemies[allEnemies.size() - 1]->getPos()].tiletype = ENEMYCHAR;
    enemyNo++;
    //return (enemy);
  };

  void			add3dSpawner(irr::scene::ISceneManager *smgr, irr::video::IVideoDriver *driver, irr::core::vector3df const &position)
  {
    mesh = smgr->getMesh("ressources/spawn.obj");
    modelNode = smgr->addAnimatedMeshSceneNode(mesh);
    if (modelNode)
      {
	std::string		tmp;
	tmp = "ressources/" + _name + ".png";
        modelNode->setScale(irr::core::vector3df(1, 1, 1));
        //modelNode->setPosition(irr::core::vector3df(-135.f, -70.f, -71.f));
	modelNode->setPosition(position);
        modelNode->setMaterialTexture(0, driver->getTexture(tmp.c_str()));
        modelNode->getMaterial(0).Shininess = 150.f;
      }
  };

  void						destroyEnemy(t_map *map, int i, int nbCol, ICharacter & player)
  {
    if (!allEnemies[i]->enemyIsAlive())
      return;
    if (nbCol > 1)
      {
	allEnemies[i]->getMesh()->setVisible(false);
	allEnemies[i]->setAlive(false);
	player.setLife(-5);
	map[allEnemies[i]->getPos()].tiletype = EMPTYCHAR;
	enemyNo--;
      }
  };

  irr::scene::IAnimatedMeshSceneNode	*	getMesh()
  {
    return (modelNode);
  };

  void						setEnemyNo(int no)
  {
    if (enemyNo > MAX_ENEMY && no > 0)
      return;
    enemyNo += no;
    if (enemyNo < 0)
      enemyNo = 0;
  };

  int						getPos()
  {
    return (pos);
  };

  void						setAlive(bool other)
  {
    isAlive -= 10;
    if (isAlive <= 0)
      {
	modelNode->setVisible(false);
	isAlive = 0;
      }
  };

  bool						enemyIsAlive()
  {
    if (isAlive > 0)
      return (true);
    return (false);
  };

  int						getEnemyNo()
  {
    return (enemyNo);
  };

  std::vector<IEnemy *> &			getEnemies()
  {
    return (allEnemies);
  };

  ~Spawner() {};
};

#endif
