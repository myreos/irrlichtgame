//
// Door.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Mon May 23 12:46:11 2016 
// Last update Sun May 29 16:16:42 2016 croayn_v
//

#ifndef DOOR_HPP_
# define DOOR_HPP_

#include <irrlicht.h>
#include <vector>
#include "ICharacter.hpp"
#include "IMusic.hpp"
#include "GetFromFile.hpp"
#include "CollisionManager.hpp"

class 			Door
{
  bool			Open;
  bool			isVertical;
  CollisionManager      colmgr;
  irr::scene::IAnimatedMesh   *mesh;
  irr::scene::IAnimatedMeshSceneNode *modelNode;
  int			allpos;
public:
  Door(t_map *map, int pos) : Open(false), isVertical(false)
  {
    int i = pos;
    allpos = pos;
    for (; map[i].tiletype != BORDERCHAR; i++)
      map[i].tiletype = DOORCHAR;
  };

  Door(t_map *map, int pos, bool vertical) : Open(false), isVertical(vertical)
  {
    int i = pos;
    allpos = pos;
    for (; map[i].tiletype != BORDERCHAR; i += BORDERMAP)
      map[i].tiletype = DOORCHAR;
  }

  void			add3dDoor(irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::core::vector3df const & scale, irr::core::vector3df const &rotation, irr::core::vector3df const & position)
  {
    mesh = smgr->getMesh("ressources/door.obj");
    modelNode = smgr->addAnimatedMeshSceneNode(mesh);
    if (modelNode)
      {
	modelNode->setScale(scale);
	modelNode->setRotation(rotation);
        modelNode->setPosition(position);
        modelNode->setMaterialTexture(0, driver->getTexture("ressources/blue-06.jpg"));
        modelNode->getMaterial(0).Shininess = 150.f;
      };
  };

  CollisionManager      &getColMgr()
  {
    return (colmgr);
  };

  irr::scene::IAnimatedMeshSceneNode	* getMesh()
  {
    return (modelNode);
  };

  bool                  isOpen(ICharacter & player, t_map * map, IMusic * backSound)
  {
    if (Open == true)
      return (true);
    colmgr.setNbCol(1);
    colmgr.collisionTwoNodes(player.getMesh(), modelNode);
    if (colmgr.getNbCol() > 1 && player.asKey() > 0)
      {
	for (int i = allpos; map[i].tiletype != BORDERCHAR; i++)
	  map[i].tiletype = EMPTYCHAR;
	Open = true;
	backSound->playSound(2);
	player.setScore(100);
	player.setKey(-1);
	modelNode->setVisible(false);
	return (true);
      }
    return (false);
  };

  bool                  isOpen(ICharacter & player, t_map * map, bool vertical, IMusic * backSound)
  {
    if (Open == true)
      return (true);
    colmgr.setNbCol(1);
    colmgr.collisionTwoNodes(player.getMesh(), modelNode);
    if (colmgr.getNbCol() > 1 && player.asKey() > 0)
      {
	for (int i = allpos; map[i].tiletype != BORDERCHAR; i += BORDERMAP)
	  map[i].tiletype = EMPTYCHAR;
	Open = true;
	player.setScore(100);
	backSound->playSound(2);
	player.setKey(-1);
	modelNode->setVisible(false);
	return (true);
      }
    return (false);
  };

  bool		vertical()
  {
    return (isVertical);
  };

  ~Door() {};
};

#endif
