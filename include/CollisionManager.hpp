//
// CollisionManager.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Sun May 22 17:28:09 2016 
// Last update Thu May 26 18:23:53 2016 
//

#ifndef COLLISIONMANAGER_HPP_
# define COLLISIONMANAGER_HPP_

#include <irrlicht.h>
#include "GetFromFile.hpp"
//#include "Spawner.hpp"

class 			CollisionManager
{
  int 			nbCol;
public:
  CollisionManager() {nbCol = 0;};
  void			collisionTwoNodes(irr::scene::IAnimatedMeshSceneNode * firstNode, irr::scene::IAnimatedMeshSceneNode * secondNode)
  {
    if (firstNode->getTransformedBoundingBox().intersectsWithBox(secondNode->getTransformedBoundingBox()))
      nbCol++;
  };

  int 				getNbCol()
  {
    return (nbCol);
  }

  void				setNbCol(int nb)
  {
    nbCol = nb;
  };

  ~CollisionManager() {};
};

#endif
