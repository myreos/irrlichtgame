//
// ICharacter.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Sat May 21 15:45:28 2016 
// Last update Tue May 31 12:45:16 2016 
//

#ifndef ICHARACTER_HPP_
# define ICHARACTER_HPP_

#include <irrlicht.h>
#include <vector>
#include "GetFromFile.hpp"
#include "IProjectile.hpp"
#define CALL_MEMBER_FN(object,ptrToMember) ((object).*(ptrToMember))
#define MOVEMENT_SPEED 3
#include "Exit.hpp"

class 					ICharacter
{
  int					playerpos;
  irr::scene::IAnimatedMesh   		*mesh;
  irr::scene::IAnimatedMeshSceneNode	*modelNode;
  typedef void 				(ICharacter::*moveVector)(int, t_map *, irr::core::vector3df&, irr::core::vector3df&, bool);
  moveVector    			*memFunPtr;
  int					key;
  int					_funcNo;
  std::vector<IProjectile*>		proj;
  int					score;
  int					life;
public:
  ICharacter() : key(false), _funcNo(0), score(0), life(100)
  {
    memFunPtr = new moveVector[13];
    for (int i = 0; i <= 12; i++)
      memFunPtr[i] = &ICharacter::idle;
    memFunPtr[1] = &ICharacter::moveLeft;
    memFunPtr[3] = &ICharacter::moveUp;
    memFunPtr[4] = &ICharacter::moveRight;
    memFunPtr[5] = &ICharacter::moveDown;
    memFunPtr[6] = &ICharacter::moveUpLeft;
    memFunPtr[10] = &ICharacter::moveUpRight;
    memFunPtr[12] = &ICharacter::moveDownRight;
    memFunPtr[8] = &ICharacter::moveDownLeft;
  };

  ICharacter(int other) : playerpos(other), key(false), _funcNo(0)
  {
    memFunPtr = new moveVector[13];
    for (int i = 0; i <= 12; i++)
      memFunPtr[i] = &ICharacter::idle;
    memFunPtr[1] = &ICharacter::moveLeft;
    memFunPtr[3] = &ICharacter::moveUp;
    memFunPtr[4] = &ICharacter::moveRight;
    memFunPtr[5] = &ICharacter::moveDown;
    memFunPtr[6] = &ICharacter::moveUpLeft;
    memFunPtr[10] = &ICharacter::moveUpRight;
    memFunPtr[12] = &ICharacter::moveDownRight;
    memFunPtr[8] = &ICharacter::moveDownLeft;
  };

  void		addNewMesh(irr::scene::ISceneManager * smgr, std::string const &meshName, std::string const &textureName, irr::video::IVideoDriver *driver)
  {
    mesh = smgr->getMesh(meshName.c_str());
    modelNode = smgr->addAnimatedMeshSceneNode(mesh);

    if (modelNode)
      {
	modelNode->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);
        modelNode->setMaterialTexture(0, driver->getTexture(textureName.c_str()));
        modelNode->getMaterial(0).Shininess = 150.f;
      }
  };

  void		setMeshPosition(irr::core::vector3df const &vect)
  {
    if (modelNode)
      modelNode->setPosition(vect);
  };

  void		setMeshRotation(irr::core::vector3df const & vect)
  {
    if (modelNode)
      modelNode->setRotation(vect);
  };

  void		setMeshScale(irr::core::vector3df const &vect)
  {
    if (modelNode)
      modelNode->setScale(vect);
  };

  void		setMeshAnim(irr::scene::EMD2_ANIMATION_TYPE anim)
  {
    if (modelNode)
      modelNode->setMD2Animation(anim);
  };

  void		idle(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
  };

  int		getPos()
  {
    return (playerpos);
  };

  void		setPos(int other)
  {
    playerpos = other;
  };

  irr::scene::IAnimatedMeshSceneNode *	getMesh()
  {
    return (modelNode);
  };

  void					moveAny(bool isRunning, t_map *map, float * playerVect, float * cameraPos, float * cameraTarget, int factor, int dist, int factor2)
  {
    if (map[playerpos + dist * factor].tiletype != EMPTYCHAR && map[playerpos + dist * factor].tiletype != ENEMYCHAR)
      return;
    map[playerpos].tiletype = EMPTYCHAR;
    map[playerpos + dist * factor].tiletype = PLAYERCHAR;
    if (isRunning == false)
      setMeshAnim(irr::scene::EMAT_RUN);
    setPos(getPos() + dist * factor);
    *playerVect += MOVEMENT_SPEED * factor2;
    *cameraPos += MOVEMENT_SPEED * factor2;
    *cameraTarget += MOVEMENT_SPEED * factor2;
  };

  void					moveUp(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    irr::core::vector3df			test = modelNode->getPosition();

    moveAny(isRunning, map, &test.Z, &cameraPos.Z, &cameraTarget.Z, -1, BORDERMAP, -1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,90,0));
  };

  void					moveDown(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    irr::core::vector3df			test = modelNode->getPosition();

    moveAny(isRunning, map, &test.Z, &cameraPos.Z, &cameraTarget.Z, 1, BORDERMAP, 1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,-90,0));
  };

  void					moveLeft(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    irr::core::vector3df			test = modelNode->getPosition();

    moveAny(isRunning, map, &test.X, &cameraPos.X, &cameraTarget.X, 1, -1, 1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,0,0));
  };

  void					moveRight(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    irr::core::vector3df			test = modelNode->getPosition();

    moveAny(isRunning, map, &test.X, &cameraPos.X, &cameraTarget.X, 1, 1, -1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,180,0));
  };

  void					moveUpRight(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    moveUp(funcNo, map, cameraPos, cameraTarget, isRunning);
    moveRight(funcNo, map, cameraPos, cameraTarget, isRunning);
    setMeshRotation(irr::core::vector3df(0,135,0));
  };

  void					moveUpLeft(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    moveUp(funcNo, map, cameraPos, cameraTarget, isRunning);
    moveLeft(funcNo, map, cameraPos, cameraTarget, isRunning);
    setMeshRotation(irr::core::vector3df(0,45,0));
  };

  void					moveDownRight(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    moveDown(funcNo, map, cameraPos, cameraTarget, isRunning);
    moveRight(funcNo, map, cameraPos, cameraTarget, isRunning);
    setMeshRotation(irr::core::vector3df(0,-135,0));
  };

  void					moveDownLeft(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    moveDown(funcNo, map, cameraPos, cameraTarget, isRunning);
    moveLeft(funcNo, map, cameraPos, cameraTarget, isRunning);
    setMeshRotation(irr::core::vector3df(0,-45,0));
  };

  void					moveSelector(int funcNo, t_map *map, irr::core::vector3df &cameraPos, irr::core::vector3df &cameraTarget, bool isRunning)
  {
    if (funcNo > 0 && funcNo <= 12)
      {
	CALL_MEMBER_FN(*this, memFunPtr[funcNo])(funcNo, map, cameraPos, cameraTarget, isRunning);
	_funcNo = funcNo;
      }
  };

  void					setKey(int other)
  {
    key += other;
  };

  int					asKey()
  {
    return (key);
  }

  int					getFuncNo()
  {
    return (_funcNo);
  };

  void					setProjectile(IProjectile *other, irr::video::IVideoDriver * driver, irr::scene::ISceneManager *smgr)
  {
    irr::core::vector3df		tmp = modelNode->getPosition();
    tmp.Y += 7;
    tmp.X -= 2;
    other->addNewMesh(smgr, "ressources/proj.obj", "ressources/orange.jpg", driver);
    other->setMeshPosition(tmp);
    other->getMesh()->setScale(irr::core::vector3df(2.2, 1.2, 1.2));
    proj.push_back(other);
  };

  template <typename T>
  int					projectileCollision(T &t, t_map *map)
  {
    int					nbDead = 0;
    for (size_t i = 0; i < proj.size(); i++)
      {
	proj[i]->getCollisionMgr().collisionTwoNodes(proj[i]->getMesh(), t->getMesh());
	if (proj[i]->getCollisionMgr().getNbCol() > 1 && proj[i]->isProjAlive() && t->enemyIsAlive())
	  {
	    proj[i]->setAlive(false);
	    //t->getMesh()->setVisible(false);
	    t->setAlive(false);
	    score += 100;
	    map[t->getPos()].tiletype = EMPTYCHAR;
	    nbDead++;

	  }
      }
    return (nbDead);
  }

  void					projectileMove(t_map *map)
  {
    for (size_t i = 0; i < proj.size(); i++)
      proj[i]->physics(map);
  };

  int					getLife()
  {
    return (life);
  };

  void					setLife(int other)
  {
    life += other;
    if (life > 100)
      life = 100;
  };

  int					getScore()
  {
    return (score);
  };

  void					setScore(int other)
  {
    score += other;
  };

  void					win(Exit & _exit, irr::video::IVideoDriver *driver)
  {
    if (_exit.getColMgr().getNbCol() > 1)
      {
	irr::video::ITexture        *irrlichtBack = driver->getTexture("ressources/win.png");
	driver->beginScene(true, true, irr::video::SColor(0, 0, 0, 0));
	if (irrlichtBack)
	  driver->draw2DImage(irrlichtBack, irr::core::position2d<int>(0, 0));
	driver->endScene();
	sleep(3);
	//backSound->playLastSound("ressources/gameover.wav", 2.f, device);
	exit(0);
      };
  };

  ~ICharacter() {};
};

#endif
