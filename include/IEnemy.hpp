//
// ICharacter.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Sat May 21 15:45:28 2016 
// Last update Tue May 31 10:21:52 2016 
//

#ifndef IENEMY_HPP_
# define IENEMY_HPP_

#include <irrlicht.h>
#include <vector>
#include "GetFromFile.hpp"
#include "CollisionManager.hpp"
#include "ICharacter.hpp"
#define CALL_MEMBER_FN(object,ptrToMember) ((object).*(ptrToMember))
#define SPEED 3

class 					IEnemy
{
  CollisionManager			colmgr;
  int					playerpos;
  bool					isAlive;
  irr::scene::IAnimatedMesh   		*mesh;
  irr::scene::IAnimatedMeshSceneNode	*modelNode;
  typedef void 				(IEnemy::*moveVector)(int, t_map *, bool);
  moveVector    			*memFunPtr;
  int way[13] = {0, -1, 0, -BORDERMAP, 1, BORDERMAP, -BORDERMAP-1, 0,
	     BORDERMAP-1, 0, -BORDERMAP+1, 0, BORDERMAP+1};
public:
  IEnemy()
  {
    isAlive = true;
    memFunPtr = new moveVector[13];
    for (int i = 0; i <= 12; i++)
      memFunPtr[i] = &IEnemy::idle;
    memFunPtr[1] = &IEnemy::moveLeft;
    memFunPtr[3] = &IEnemy::moveUp;
    memFunPtr[4] = &IEnemy::moveRight;
    memFunPtr[5] = &IEnemy::moveDown;
    memFunPtr[6] = &IEnemy::moveUpLeft;
    memFunPtr[10] = &IEnemy::moveUpRight;
    memFunPtr[12] = &IEnemy::moveDownRight;
    memFunPtr[8] = &IEnemy::moveDownLeft;
  };

  IEnemy(int other) : playerpos(other)
  {
    isAlive = true;
    memFunPtr = new moveVector[13];
    for (int i = 0; i <= 12; i++)
      memFunPtr[i] = &IEnemy::idle;
    memFunPtr[1] = &IEnemy::moveLeft;
    memFunPtr[3] = &IEnemy::moveUp;
    memFunPtr[4] = &IEnemy::moveRight;
    memFunPtr[5] = &IEnemy::moveDown;
    memFunPtr[6] = &IEnemy::moveUpLeft;
    memFunPtr[10] = &IEnemy::moveUpRight;
    memFunPtr[12] = &IEnemy::moveDownRight;
    memFunPtr[8] = &IEnemy::moveDownLeft;
  };

  float   distance(int pos, int stop)
  {
    return (sqrt(pow((pos % BORDERMAP) - (stop % BORDERMAP), 2) +
		 pow((pos/BORDERMAP) - (stop/BORDERMAP), 2)));
  }

  void		addNewMesh(irr::scene::ISceneManager * smgr, std::string const &meshName, std::string const &textureName, irr::video::IVideoDriver *driver)
  {
    mesh = smgr->getMesh(meshName.c_str());
    modelNode = smgr->addAnimatedMeshSceneNode(mesh);

    if (modelNode)
      {
	modelNode->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);
        modelNode->setMaterialTexture(0, driver->getTexture(textureName.c_str()));
        modelNode->getMaterial(0).Shininess = 150.f;
      }
  };

  void		setMeshPosition(irr::core::vector3df const &vect)
  {
    if (modelNode)
      modelNode->setPosition(vect);
  };

  void		setMeshRotation(irr::core::vector3df const & vect)
  {
    if (modelNode)
      modelNode->setRotation(vect);
  };

  void		setMeshScale(irr::core::vector3df const &vect)
  {
    if (modelNode)
      modelNode->setScale(vect);
  };

  void		setMeshAnim(irr::scene::EMD2_ANIMATION_TYPE anim)
  {
    if (modelNode)
      modelNode->setMD2Animation(anim);
  };

  void		idle(int funcNo, t_map *map, bool isRunning)
  {
  };

  int		getPos()
  {
    return (playerpos);
  };

  void			setPos(int other)
  {
    playerpos = other;
  };

  irr::scene::IAnimatedMeshSceneNode *	getMesh()
  {
    return (modelNode);
  };

  void					moveAny(bool isRunning, t_map *map, float * playerVect, int factor, int dist, int factor2)
  {
    if (map[playerpos + dist * factor].tiletype != EMPTYCHAR && map[playerpos + dist * factor].tiletype != PLAYERCHAR && map[playerpos + dist * factor].tiletype != ENEMYCHAR)
      return;
    map[playerpos].tiletype = EMPTYCHAR;
    map[playerpos + dist * factor].tiletype = ENEMYCHAR;
    if (isRunning == false)
      setMeshAnim(irr::scene::EMAT_RUN);
    setPos(getPos() + dist * factor);
    *playerVect += SPEED * factor2;
  };

  void					moveUp(int funcNo, t_map *map, bool isRunning)
  {
    irr::core::vector3df			test = modelNode->getPosition();

    moveAny(isRunning, map, &test.Z, -1, BORDERMAP, -1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,90,0));
  };

  void					moveDown(int funcNo, t_map *map, bool isRunning)
  {
    irr::core::vector3df			test = modelNode->getPosition();

    moveAny(isRunning, map, &test.Z, 1, BORDERMAP, 1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,-90,0));
  };

  void					moveLeft(int funcNo, t_map *map, bool isRunning)
  {
    irr::core::vector3df			test = modelNode->getPosition();

    moveAny(isRunning, map, &test.X, 1, -1, 1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,0,0));
  };

  void					moveRight(int funcNo, t_map *map, bool isRunning)
  {
    irr::core::vector3df			test = modelNode->getPosition();

    moveAny(isRunning, map, &test.X, 1, 1, -1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,180,0));
  };

  void					moveUpRight(int funcNo, t_map *map, bool isRunning)
  {
    moveUp(funcNo, map, isRunning);
    moveRight(funcNo, map, isRunning);
    setMeshRotation(irr::core::vector3df(0,135,0));
  };

  void					moveUpLeft(int funcNo, t_map *map, bool isRunning)
  {
    moveUp(funcNo, map, isRunning);
    moveLeft(funcNo, map, isRunning);
    setMeshRotation(irr::core::vector3df(0,45,0));
  };

  void					moveDownRight(int funcNo, t_map *map, bool isRunning)
  {
    moveDown(funcNo, map, isRunning);
    moveRight(funcNo, map, isRunning);
    setMeshRotation(irr::core::vector3df(0,-135,0));
  };

  void					moveDownLeft(int funcNo, t_map *map, bool isRunning)
  {
    moveDown(funcNo, map, isRunning);
    moveLeft(funcNo, map, isRunning);
    setMeshRotation(irr::core::vector3df(0,-45,0));
  };

  void					moveSelector(int funcNo, t_map *map, bool isRunning)
  {
    if (funcNo > 0 && funcNo <= 12)
      CALL_MEMBER_FN(*this, memFunPtr[funcNo])(funcNo, map, isRunning);
  };

  void					ia(ICharacter & player, t_map *map)
  {
    float			       	dist;
    int 				next_mv;

    if (!isAlive)
      return;
    dist = distance(playerpos, player.getPos());
    for (int cpt = 0; cpt < 13; cpt++)
      {
        if (distance(playerpos + way[cpt], player.getPos()) < dist)
          {
            dist = distance(playerpos + way[cpt], player.getPos());
            next_mv = cpt;
          }
      }
    moveSelector(next_mv, map, true);
  };

  void					setAlive(bool other)
  {
    modelNode->setVisible(false);
    isAlive = other;
  };

  bool					isRunning()
  {
    return (true);
  }

  bool					enemyIsAlive()
  {
    return (isAlive);
  };

  CollisionManager	&		getCollisionMgr()
  {
    return (colmgr);
  };

  ~IEnemy() {};
};

#endif
