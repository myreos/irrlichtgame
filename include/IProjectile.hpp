//
// IProjectile.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by
// Login   <grandr_r@epitech.net>
//
// Started on  Mon May 23 15:23:25 2016
// Last update Thu May 26 11:05:39 2016 
//

#ifndef IPROJECTILE_HPP_
# define IPROJECTILE_HPP_

#include <irrlicht.h>
#include <vector>
#include "GetFromFile.hpp"
#include "CollisionManager.hpp"
#define CALL_MEMBER_FN(object,ptrToMember) ((object).*(ptrToMember))
#define MOVEMENT_SPEED2 7

class                                   IProjectile
{
  CollisionManager                      colmgr;
  int                                   playerpos;
  bool                                  isAlive;
  irr::scene::IAnimatedMesh             *mesh;
  irr::scene::IAnimatedMeshSceneNode    *modelNode;
  typedef void                          (IProjectile::*moveVector)(int, t_map *, bool);
  moveVector                            *memFunPtr;
  int                                   dirVector;
public:
  IProjectile(int dir) : dirVector(dir)
  {
    isAlive = true;
    memFunPtr = new moveVector[13];
    for (int i = 0; i <= 12; i++)
      memFunPtr[i] = &IProjectile::idle;
    memFunPtr[1] = &IProjectile::moveLeft;
    memFunPtr[3] = &IProjectile::moveUp;
    memFunPtr[4] = &IProjectile::moveRight;
    memFunPtr[5] = &IProjectile::moveDown;
    memFunPtr[6] = &IProjectile::moveUpLeft;
    memFunPtr[10] = &IProjectile::moveUpRight;
    memFunPtr[12] = &IProjectile::moveDownRight;
    memFunPtr[8] = &IProjectile::moveDownLeft;
  };

  IProjectile(int other, int dir) : playerpos(other), dirVector(dir)
  {
    isAlive = true;
    memFunPtr = new moveVector[13];
    for (int i = 0; i <= 12; i++)
      memFunPtr[i] = &IProjectile::idle;
    memFunPtr[1] = &IProjectile::moveLeft;
    memFunPtr[3] = &IProjectile::moveUp;
    memFunPtr[4] = &IProjectile::moveRight;
    memFunPtr[5] = &IProjectile::moveDown;
    memFunPtr[6] = &IProjectile::moveUpLeft;
    memFunPtr[10] = &IProjectile::moveUpRight;
    memFunPtr[12] = &IProjectile::moveDownRight;
    memFunPtr[8] = &IProjectile::moveDownLeft;
  };

  void          addNewMesh(irr::scene::ISceneManager * smgr, std::string const &meshName, std::string const &textureName, irr::video::IVideoDriver *driver)
  {
    mesh = smgr->getMesh(meshName.c_str());
    modelNode = smgr->addAnimatedMeshSceneNode(mesh);

    if (modelNode)
      {
        modelNode->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);
        modelNode->setMaterialTexture(0, driver->getTexture(textureName.c_str()));
        modelNode->getMaterial(0).Shininess = 150.f;
      }
  };

  void          setMeshPosition(irr::core::vector3df const &vect)
  {
    if (modelNode)
      modelNode->setPosition(vect);
  };

  void          setMeshRotation(irr::core::vector3df const & vect)
  {
    if (modelNode)
      modelNode->setRotation(vect);
  };

  void          setMeshScale(irr::core::vector3df const &vect)
  {
    if (modelNode)
      modelNode->setScale(vect);
  };

  void          setMeshAnim(irr::scene::EMD2_ANIMATION_TYPE anim)
  {
    if (modelNode)
      modelNode->setMD2Animation(anim);
  };

  void          idle(int funcNo, t_map *map, bool isRunning)
  {
    modelNode->setVisible(false);
  };

  int           getPos()
  {
    return (playerpos);
  };

  void                  setPos(int other)
  {
    playerpos = other;
  };

  irr::scene::IAnimatedMeshSceneNode *  getMesh()
  {
    return (modelNode);
  };

  void                                  moveAny(bool isRunning, t_map *map, float * playerVect, int factor, int dist, int factor2)
  {
    setPos(getPos() + dist * factor);
    *playerVect += MOVEMENT_SPEED2 * factor2;
  };

  void                                  moveUp(int funcNo, t_map *map, bool isRunning)
  {
    irr::core::vector3df                        test = modelNode->getPosition();

    moveAny(isRunning, map, &test.Z, -1, BORDERMAP, -1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,90,0));
  };

  void                                  moveDown(int funcNo, t_map *map, bool isRunning)
  {
    irr::core::vector3df                        test = modelNode->getPosition();

    moveAny(isRunning, map, &test.Z, 1, BORDERMAP, 1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,-90,0));
  };

  void                                  moveLeft(int funcNo, t_map *map, bool isRunning)
  {
    irr::core::vector3df                        test = modelNode->getPosition();

    moveAny(isRunning, map, &test.X, 1, -1, 1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,0,0));
  };

  void                                  moveRight(int funcNo, t_map *map, bool isRunning)
  {
    irr::core::vector3df                        test = modelNode->getPosition();

    moveAny(isRunning, map, &test.X, 1, 1, -1);
    setMeshPosition(test);
    setMeshRotation(irr::core::vector3df(0,180,0));
  };

  void                                  moveUpRight(int funcNo, t_map *map, bool isRunning)
  {
    moveUp(funcNo, map, isRunning);
    moveRight(funcNo, map, isRunning);
    setMeshRotation(irr::core::vector3df(0,135,0));
  };

  void                                  moveUpLeft(int funcNo, t_map *map, bool isRunning)
  {
    moveUp(funcNo, map, isRunning);
    moveLeft(funcNo, map, isRunning);
    setMeshRotation(irr::core::vector3df(0,45,0));
  };

  void                                  moveDownRight(int funcNo, t_map *map, bool isRunning)
  {
    moveDown(funcNo, map, isRunning);
    moveRight(funcNo, map, isRunning);
    setMeshRotation(irr::core::vector3df(0,-135,0));
  };

  void                                  moveDownLeft(int funcNo, t_map *map, bool isRunning)
  {
    moveDown(funcNo, map, isRunning);
    moveLeft(funcNo, map, isRunning);
    setMeshRotation(irr::core::vector3df(0,-45,0));
  };

  void                                  moveSelector(int funcNo, t_map *map, bool isRunning)
  {
    if (funcNo > 0 && funcNo <= 12)
      CALL_MEMBER_FN(*this, memFunPtr[funcNo])(funcNo, map, isRunning);
  };

  bool					isProjAlive()
  {
    return (isAlive);
  };

  void                                  setAlive(bool other)
  {
    if (!isAlive)
      return;
    isAlive = other;
    modelNode->setVisible(false);
  };

  void                                  physics(t_map *map)
  {
    if (isAlive)
      moveSelector(dirVector, map, false);
  };

  CollisionManager      &               getCollisionMgr()
  {
    return (colmgr);
  };

  ~IProjectile() {};
};

#endif
