//
// Key.hpp for  in /home/grandr_r/Cpp_Prog/cpp_indie_studio
//
// Made by 
// Login   <grandr_r@epitech.net>
//
// Started on  Mon May 23 10:36:50 2016 
// Last update Sat May 28 16:57:42 2016 
//

#ifndef CHEST_HPP_
# define CHEST_HPP_

#include <irrlicht.h>
#include "IMusic.hpp"
#include "ICharacter.hpp"
#include "CollisionManager.hpp"

class 			Chest
{
  bool			isAvailable;
  CollisionManager	colmgr;
  irr::scene::IAnimatedMesh   *mesh;
  irr::scene::IAnimatedMeshSceneNode *modelNode;
public:
  Chest() : isAvailable(true) {};
  void			add3dChest(irr::video::IVideoDriver *driver, irr::scene::ISceneManager *smgr, irr::core::vector3df const &position)
  {
    mesh = smgr->getMesh("ressources/chest.obj");
    modelNode = smgr->addAnimatedMeshSceneNode(mesh);
    if (modelNode)
      {
	modelNode->setScale(irr::core::vector3df(6, 6, 6));
        //modelNode->setPosition(irr::core::vector3df(-140.f, -70.f, 0.f));
	modelNode->setPosition(position);
        modelNode->setMaterialTexture(0, driver->getTexture("ressources/chest.jpg"));
        modelNode->getMaterial(0).Shininess = 150.f;
      }
  };

  CollisionManager	&getColMgr()
  {
    return (colmgr);
  };

  irr::scene::IAnimatedMeshSceneNode *	getMesh()
  {
    return (modelNode);
  };

  bool			isTaken(ICharacter & player, IMusic * backSound)
  {
    if (isAvailable == false)
      return (false);
    if (colmgr.getNbCol() > 1)
      {
	isAvailable = false;
	backSound->playSound(0);
	player.setScore(100);
	modelNode->setVisible(false);
	return (true);
      }
    return (false);
  };

  ~Chest() {};
};

#endif
